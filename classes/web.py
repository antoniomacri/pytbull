#!/usr/bin/env python
#-*- coding: utf-8 -*-

import cherrypy
import database
import re
import operator
import os

class Web:

    def __init__(self, data_dir, default_db = None):
        self.data_dir = data_dir
        self.default_db = default_db

    def footer(self):
        footer = """
            <div id="footer">
                <div style="padding:10px;">pytbull is developed and maintained by S&eacute;bastien Damaye</div>
                <div><a href="http://pytbull.sourceforge.net" style="color:#fff;">pytbull.sf.net</a>&nbsp;|&nbsp;<a href="http://www.aldeid.com" style="color:#fff;">aldeid.com</a></div>
            </div>
            </body>
            </html>"""
        return footer

    def flag2title(self, flag):
        t = ''
        if flag==0:
            t = 'no detection'
        elif flag == 1:
            t = 'partial detection'
        elif flag==2:
            t = 'full detection'
        return t

    def get_db_list_items(self, current_db = None, page = '/'):
        db_list = os.listdir(self.data_dir)
        db_list = filter(lambda e: e.endswith('.db'), db_list)
        db_list = map(lambda e: e[:-3], db_list)
        result = ""
        for db in db_list[::-1]:
            if db == current_db:
                (left, right) = ("<b>", "</b>")
            else:
                (left, right) = ("", "")
            result += """<li class="db"><a href="%s?db=%s">%s%s%s</a></li>""" % (page, db, left, db, right)
        if len(db_list) <= 0:
            result += """<li class="db"><a><i>(no database found)</i></a></li>"""
        return result

    def check_db(self, requested_db, page = '/'):
        db_list = os.listdir(self.data_dir)
        if requested_db:
            db = requested_db
        else:
            db = self.default_db
        index = -1
        try:
            index = db_list.index(db + ".db")
        except:
            pass
        if index >= 0:
            return (db, os.path.join(self.data_dir, db_list[index]), None)
        if db:
            error_msg = """<h1>An error occurred :-(</h1>
                The specified database '%s' does not exist. Please specify a valid database, or choose one from the list above.""" % db
        else:
            error_msg = """<h1>Welcome</h1>
                Please choose the database you want me to display from the list above."""
        result = """
            <!DOCTYPE html>
            <html>
            <head>
              <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
              <title>pytbull report</title>
              <link rel="stylesheet" type="text/css" href="/styles2.css" />
            </head>
            <body>
              <div id="container">
                <div id="header">
                  <div><a href="/" title="home"><img src="/img/logo.png" alt="pytbull logo" /></a></div>
                  <ul id="menu">%s</ul>
                </div>
                <div style="clear:both"></div>
                <div id="content">%s</div>""" % (self.get_db_list_items(page = page), error_msg)
        result += self.footer()
        return (db, None, result)

    @cherrypy.expose
    def csv(self, db = None):
        (db, db_path, result) = self.check_db(db)
        if not db_path:
            return result;

        cherrypy.response.headers['Content-Type'] = "text/plain"
        cherrypy.response.headers['Content-Disposition'] = "inline;filename=%s.csv" % db

        # Tables
        text = "category,teststotal,testsgreen,testsorange,testsred,testsblack,alertstotal,alertsgreen,alertsorange,alertsgray,alertsblack\n"
        sums = [0] * 10
        for s in database.DB(db_path).getStatsModulesTabular():
            text += ",".join(map(str, s)) + "\n"
            sums = map(operator.add, sums, s[1:])
        text += "total," + ",".join(map(str, sums)) + "\n"
        return text

    @cherrypy.expose
    def index(self, db = None):
        (db, db_path, result) = self.check_db(db)
        if not db_path:
            return result;

        index ="""
            <!DOCTYPE html>
            <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
                <title>pytbull report</title>
                <script type="text/javascript" src="/js/jquery.js"></script>
                <script type="text/javascript" src="/js/jquery.jqplot.js"></script>
                <script type="text/javascript" src="/js/plugins/jqplot.pieRenderer.js"></script>
                <script type="text/javascript" src="/js/plugins/jqplot.donutRenderer.js"></script>
                <script type="text/javascript" src="/js/plugins/jqplot.dateAxisRenderer.js"></script>
                <script type="text/javascript" src="/js/plugins/jqplot.canvasTextRenderer.js"></script>
                <script type="text/javascript" src="/js/plugins/jqplot.canvasAxisTickRenderer.js"></script>
                <script type="text/javascript" src="/js/plugins/jqplot.categoryAxisRenderer.js"></script>
                <script type="text/javascript" src="/js/plugins/jqplot.barRenderer.js"></script>
                <link rel="stylesheet" type="text/css" href="/js/jquery.jqplot.css" />
                <link rel="stylesheet" type="text/css" href="/styles2.css" />"""
        testresults = database.DB(db_path).getStatsTestsResults()
        index += """<script type="text/javascript">
            $(document).ready(function(){
              var data = [
                ['full detection', %d],['partial detection', %d], ['no detection', %d]
              ];
              var plot1 = jQuery.jqplot ('chart1', [data],
                {
                  title: 'Tests results',
                  seriesColors: ["#61C200", "#FF8000", "#ff0000"],
                  seriesDefaults: {
                    renderer: jQuery.jqplot.PieRenderer,
                    rendererOptions: {
                      showDataLabels: true
                    }
                  },
                  legend: { show:true, location: 'e' }
                }
              );
            });
            </script>""" % (testresults[1], testresults[2], testresults[3])
        moddistrib = database.DB(db_path).getStatsModulesDistribution()
        s = "["
        for i in moddistrib:
            s += "['%s', %d]," % (i[0], i[1])
        s = s[:-1] + "]"
        index += """<script type="text/javascript">
            $(document).ready(function(){
                var line1 = %s;

                var plot1 = $.jqplot('chart2', [line1], {
                title: 'Modules distribution',
                series:[{renderer:$.jqplot.BarRenderer}],
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                    tickOptions: {
                      angle: -30,
                      fontSize: '10pt'
                    }
                },
                axes: {
                  xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer
                  }
                }
                });
            });
            </script>""" % s

        # Donuts for each module
        for i in moddistrib:
            res = database.DB(db_path).getTestDistribModule(i[0])
            if sum(r[1] for r in res)!=0:
                index += """<script type="text/javascript">
                    $(document).ready(function(){"""
                index += """var s1 = ["""
                for r in res:
                    index += "['%s',%d]," % (self.flag2title(r[0]), r[1])
                index += "];"
                index += """var plot3 = $.jqplot('%s', [s1], {
                        title: '%s',
                        seriesColors: ["#61C200", "#FF8000", "#ff0000"],
                        seriesDefaults: {
                          // make this a donut chart.
                          renderer:$.jqplot.DonutRenderer,
                          rendererOptions:{
                            // Donut's can be cut into slices like pies.
                            sliceMargin: 3,
                            // Pies and donuts can start at any arbitrary angle.
                            startAngle: -90,
                            showDataLabels: true,
                            // By default, data labels show the percentage of the donut/pie.
                            // You can show the data 'value' or data 'label' instead.
                            dataLabels: 'value'
                          }
                        },
                        legend: {
                          show: true,
                          location: 'e',
                          placement: 'inside'
                        }
                      });
                    });
                    </script>""" % (i[0], i[0])

        index += """</head>
            <body>"""
        index += """<div id="container">
            <div id="header">
                <div><a href="/" title="home"><img src="/img/logo.png" alt="pytbull logo" /></a></div>
                <ul id="menu">
                    <li><a href="/?db=%s">Stats</a></li>
                    <li><a href="/details?db=%s">Details</a></li>
                    <li><a href="/search?db=%s">Search</a></li>
                    %s
                </ul>
            </div>
            <div style="clear:both"></div>
            <div id="content">""" % (db, db, db, self.get_db_list_items(db))
        index += """<h1>Global stats</h1>
            <div id="chart1" style="float:left;height:340px;width:480px;"></div>
            <div id="chart2" style="float:left;height:340px;width:480px;"></div>
            <div style="clear:both"></div>"""

        # Donuts
        index += """<h1>Modules stats</h1>"""
        for i in moddistrib:
            res = database.DB(db_path).getTestDistribModule(i[0])
            if sum(r[1] for r in res)!=0:
                index += """<div id="%s" style="width:320px;height:230px;float:left"></div>""" % i[0]
        index += """<div style="clear:both;"></div>"""

        # Tables
        index += """<h1>Tabular stats</h1>"""
        index += """\n<table border="1" style="width:960px;">
            <tr>
                <th rowspan="2">Category</th>
                <th colspan="5">Tests</th>
                <th colspan="5">Alerts</th>
            </tr>
            <tr>
                <th>Total</th>
                <th><img src="/img/bullet_green.png" alt="tests passed" /></th>
                <th><img src="/img/bullet_orange.png" alt="tests partially passed" /></th>
                <th><img src="/img/bullet_red.png" alt="tests failed" /></th>
                <th><img src="/img/bullet_black.png" alt="tests with false positives" /></th>
                <th>Total</th>
                <th><img src="/img/bullet_green.png" alt="green alerts" /></th>
                <th><img src="/img/bullet_orange.png" alt="orange alerts" /></th>
                <th><img src="/img/bullet_gray.png" alt="gray alerts" /></th>
                <th><img src="/img/bullet_black.png" alt="black alerts" /></th>
            </tr>"""

        sums = [0] * 10
        for s in database.DB(db_path).getStatsModulesTabular():
            index += "\n<tr>"
            index += """\n<td>%s</td>""" % s[0]
            index += "".join(map(lambda e: """\n<td>%d</td>""" % e, s[1:]))
            index += "\n</tr>"
            sums = map(operator.add, sums, s[1:])
        index += "\n<tr>"
        index += """\n<td><b>%s</b></td>""" % "Total"
        for s in sums:
            index += """\n<td><b>%d</b></td>""" % s
        index += "\n</tr>"
        index += """\n</table>"""

        # Occurrences
        alerts = []
        for test in database.DB(db_path).listTests():
            test_alert = test[12]
            lines = test_alert.split("\n")
            for line in lines:
                if not line:
                    continue
                if "[**]" in line:
                    alerts.append(line)
                else:
                    alerts[len(alerts)-1] += line + "\n"
        if len(alerts) > 0:
            index += """<h1>Alerts occurrences</h1>"""
            index += """\n<table border="1" style="width:960px;">"""
            index += """<tr><th>#</th><th>Description</th></tr>"""
            occurrences = {}
            re_date = re.compile(r"\d+/\d+(?:/\d+)?-\d+:\d+:\d+\.\d+\s+\[\*\*\]\s*(.+)")
            re_ip_port = re.compile(r"(\d+\.\d+\.\d+\.\d+:)\d+")
            alerts_notimestamp = [ re_ip_port.sub("\\1*", re_date.match(x).group(1)) for x in alerts ]
            for a in alerts_notimestamp:
                occurrences[a] = occurrences.get(a, 0) + 1
            for (k,v) in sorted(occurrences.items(), key=operator.itemgetter(1), reverse=True):
                index += """<tr><td>%s</td><td>%s</td></tr>""" % (v,k)
            index += """\n</table>"""

        index += "</div>"
        index += self.footer()
        return index

    @cherrypy.expose
    def details(self, db = None, description = None, module = None, port = None, proto = None, payload_fmt = None, test_result = None, payload = None, alert = None):
        (db, db_path, result) = self.check_db(db, "details")
        if not db_path:
            return result;

        index ="""
            <!DOCTYPE html>
            <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
                <title>pytbull report</title>
                <link rel="stylesheet" type="text/css" href="/styles2.css" />
                <script type="text/javascript">
                    function expandcollapse(id) {
                        var browser = navigator.appName;
                        if(browser == "Netscape"){
                            var showstr = 'table-row';
                        } else {
                            var showstr = 'block';
                        }
                        document.getElementById(id).style.display = (document.getElementById(id).style.display=='none')?showstr:'none';
                        document.getElementById('img_'+id).src = (document.getElementById(id).style.display=='none')?'/img/expand.png':'/img/collapse.png';
                    }
                </script>
            </head>
            <body>"""
        index += """<div id="container">
            <div id="header">
                <div><a href="/" title="home"><img src="/img/logo.png" alt="pytbull logo" /></a></div>
                <ul id="menu">
                    <li><a href="/?db=%s">Stats</a></li>
                    <li><a href="/details?db=%s">Details</a></li>
                    <li><a href="/search?db=%s">Search</a></li>
                    %s
                </ul>
            </div>
            <div style="clear:both"></div>
            <div id="content">""" % (db, db, db, self.get_db_list_items(db, "details"))

        ### Filters
        filters = []
        port, proto, payload_fmt, test_result, payload, alert
        if description!=None and description!= '':
            filters.append(["Description", description])
        if module!=None and module!='any':
            filters.append(["Module", module])
        if port!=None and port!='':
            filters.append(["Port", port])
        if proto!=None and proto!='any':
            filters.append(["Proto", proto])
        if payload_fmt!=None and payload_fmt!='any':
            filters.append(["Format", payload_fmt])
        if test_result!=None and test_result!='any':
            if test_result == "0":
                res = "no detection"
            elif test_result=="1":
                res = "partial detection"
            elif test_result=="2":
                res = "full detection"
            filters.append(["Result", res])
        if payload!=None and payload!='':
            filters.append(["Payload", payload])
        if alert!=None and alert!='':
            filters.append(["Alert", alert])
        #display filters
        if len(filters) != 0:
            index += """<div style="padding:5px;">"""
            index += """<div style="float:left;padding:3px;"><strong>Filters: </strong></div>"""
            for f in filters:
                index += """<div style="float:left;padding:2px;margin-left:5px;border:solid 1px #284655; background:#93C4D9;-moz-border-radius:5px;border-radius:5px;">%s=%s</div>""" % (f[0], f[1])
            index += """<div style="clear:both"></div></div>"""

        index += """\n<table border="1" style="width:960px;">
            <tr>
                <th></th>
                <th>#</th>
                <th>Description</th>
                <th>Module</th>
                <th>Port</th>
                <th>Payload fmt</th>
                <th># of alerts</th>
                <th><img src="/img/bullet_green.png" alt="green alerts" /></th>
                <th><img src="/img/bullet_orange.png" alt="orange alerts" /></th>
                <th><img src="/img/bullet_black.png" alt="black alerts" /></th>
                <th>Result</th>
            </tr>"""
        for test in database.DB(db_path).listTests(description, module, port, proto, payload_fmt, test_result, payload, alert):
            test_sig_green = test[9]
            test_sig_orange = test[10]
            test_sig_black = test[11]
            test_alert = test[12]
            test_flag = test[13]
            lines = test_alert.split("\n")
            alerts = []
            for line in lines:
                if not line:
                    continue
                if "[**]" in line:
                    alerts.append(line)
                else:
                    alerts[len(alerts)-1] += line + "\n"
            green_alerts = []
            orange_alerts = []
            black_alerts = []
            unknown_alerts = []
            for x in alerts:
                if test_sig_green and re.search(test_sig_green, x):
                    green_alerts.append(x)
                elif test_sig_orange and re.search(test_sig_orange, x):
                    orange_alerts.append(x)
                elif test_sig_black and re.search(test_sig_black, x):
                    black_alerts.append(x)
                else:
                    unknown_alerts.append(x)
            index += """\n<tr id="tr_%d">""" % test[0]
            # Expand / collapse
            index += """\n<td><a href="javascript:expandcollapse(%d)"><img id="img_%d" src="/img/expand.png" alt="expand/collapse"/></a></td>""" % (test[0],test[0])
            # id_test
            index += """\n<td>%d</td>""" % test[0]
            # test_name
            index += """\n<td>%s</td>""" % test[5]
            # test_module
            index += """\n<td>%s</td>""" % test[1]
            # port / proto
            if test[6] and test[7]:
                index += """\n<td>%s/%s</td>""" % (test[6], test[7])
            elif test[6] or test[7]:
                index += """\n<td>%s</td>""" % (test[6] or test[7])
            else:
                index += """\n<td></td>"""
            # Payload format (Test type)
            index += """\n<td>%s</td>""" % test[2]
            # Number of alerts
            index += """\n<td>%d</td>""" % len(alerts)
            # Number of green alerts
            index += """\n<td>%d</td>""" % len(green_alerts)
            # Number of orange alerts
            index += """\n<td>%d</td>""" % len(orange_alerts)
            # Number of black alerts
            index += """\n<td>%d</td>""" % len(black_alerts)
            # test flag
            if test[9]!=None:
                index += """\n<td><img src="/img/traffic_light_%s.png" alt="traffic light" /></td>""" % test_flag
            else:
                index += """\n<td></td>"""
            index += "\n</tr>"

            index += """\n<tr id="%d" style="display:none;"><td colspan="11" style="border:solid 3px #284655;background:#E9ECF0;">""" % test[0]
            index += "<ul>"
            index += """<li><strong>Start:</strong> %s</li>""" % test[3]
            index += """<li><strong>End:</strong> %s</li>""" % test[4]
            index += """<li><strong>Sig green:</strong> %s</li>""" % test_sig_green
            index += """<li><strong>Sig orange:</strong> %s</li>""" % test_sig_orange
            index += """<li><strong>Sig black:</strong> %s</li>""" % test_sig_black
            index += "</ul>"
            if test[8]!='' and test[8]!=None:
                index += """<div><strong>Payload:</strong></div>"""
                try:
                    index += """<div><textarea style="width:98%%;height:70px;">%s</textarea></div>""" % test[8]
                except:
                    index += """<div>***Error: Can not be displayed</div>"""
                index += "&nbsp;"
            if len(alerts) > 0:
                index += """<div><strong>Alerts:</strong></div>"""
                index += """<div><textarea style="width:98%%;height:200px;">%s</textarea></div>""" % "\n".join(alerts)
                index += "&nbsp;"
            else:
                index += """<div><strong>Alerts:</strong> none</div>"""

            if len(alerts) > 0:
                index += "&nbsp;"
                index += "<table>\n"
                index += """<tr><th>#</th><th>Description</th><th></th></tr>"""
                occurrences = {}
                re_date = re.compile(r"\d+/\d+(?:/\d+)?-\d+:\d+:\d+\.\d+\s+\[\*\*\]\s*(.+)")
                re_ip_port = re.compile(r"(\d+\.\d+\.\d+\.\d+:)\d+")
                alerts_notimestamp = [ re_ip_port.sub("\\1*", re_date.match(x).group(1)) for x in alerts ]
                for a in alerts_notimestamp:
                    occurrences[a] = occurrences.get(a, 0) + 1
                for (k,v) in sorted(occurrences.items(), key=operator.itemgetter(1), reverse=True):
                    bullet = test_sig_green and re.search(test_sig_green, k) and "green" or \
                             test_sig_orange and re.search(test_sig_orange, k) and "orange" or \
                             test_sig_black and re.search(test_sig_black, k) and "black" or \
                             None
                    index += """<tr><td>%s</td><td>%s</td><td>%s</td></tr>""" % (v, k, """<img src='/img/bullet_%s.png' />""" % bullet if bullet else "")
                index += """\n</table>"""

            index += """</td></tr>"""

        index += """\n</table>"""
        index += """</div>"""
        index += self.footer()
        return index

    @cherrypy.expose
    def search(self, db = None):
        (db, db_path, result) = self.check_db(db, "search")
        if not db_path:
            return result;

        index ="""
            <!DOCTYPE html>
            <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
                <title>pytbull report</title>
                <link rel="stylesheet" type="text/css" href="/styles2.css" />
            </head>
            <body>"""
        index += """<div id="container">
            <div id="header">
                <div><a href="/" title="home"><img src="/img/logo.png" alt="pytbull logo" /></a></div>
                <ul id="menu">
                    <li><a href="/?db=%s">Stats</a></li>
                    <li><a href="/details?db=%s">Details</a></li>
                    <li><a href="/search?db=%s">Search</a></li>
                    %s
                </ul>
            </div>
            <div style="clear:both"></div>
            <div id="content">""" % (db, db, db, self.get_db_list_items(db, "search"))
        index += """
            <form action="/details" method="get">
                <input type="hidden" name="db" value="%s" />
                <table><tr><td style="vertical-align:top;width:350px;">""" % db

        ### Description
        index += """
                <div style="float:left;width:100px;"><strong>Description</strong></div>
                <div style="float:left"><input type="text" name="description" style="width:200px;" /></div>
                <div style="clear:both"></div>"""

        ### Modules
        index += """
                <div style="float:left;width:100px;"><strong>Modules*</strong></div>
                <div style="float:left">
                    <select name="module">
                        <option value="any">any</option>"""
        modules = database.DB(db_path).getStatsModulesDistribution()
        for module in modules:
            index += """<option value="%s">%s</option>""" % (module[0], module[0])
        index += """</select>"""
        index += """
                </div>
                <div style="clear:both"></div>"""
                
        ### Port / proto
        index += """
                <div style="float:left;width:100px;"><strong>Port/Proto</strong></div>
                <div style="float:left"><input type="text" name="port" style="width:30px;" /></div>
                <div style="float:left">/</div>
                <div style="float:left">
                    <select name="proto">
                        <option value="any">any</option>
                        <option value="tcp">tcp</option>
                        <option value="udp">udp</option>
                    </select>
                </div>
                <div style="clear:both"></div>"""

        ### Separator
        index += """</td><td style="vertical-align:top;">"""

        ### Payload formats
        index += """
                <div style="float:left;width:100px;"><strong>Payload format</strong></div>
                <div style="float:left">
                    <select name="payload_fmt">
                        <option value="any">any</option>"""
        payload_formats = database.DB(db_path).getPayloadFormats()
        for fmt in payload_formats:
            index += """<option value="%s">%s</option>""" % (fmt[0], fmt[0])
        index += """
                    </select>
                </div>
                <div style="clear:both"></div>"""

        ### Test results
        index += """
                <div style="float:left;width:100px;"><strong>Test result</strong></div>
                <div style="float:left">
                    <select name="test_result">
                        <option value="any">any</option>
                        <option value="2">full detection</option>
                        <option value="1">partial detection</option>
                        <option value="0">no detection</option>
                    </select>
                </div>
                <div style="clear:both"></div>"""

        ### Payload
        index += """
                <div style="float:left;width:100px;"><strong>Payload</strong></div>
                <div style="float:left"><input type="text" name="payload" style="width:200px;" /></div>
                <div style="clear:both"></div>"""

        ### Alert
        index += """
                <div style="float:left;width:100px;"><strong>Alert</strong></div>
                <div style="float:left"><input type="text" name="alert" style="width:200px;" /></div>
                <div style="clear:both"></div>"""

        ### End of form
        index += """
                </td></tr>
                <tr><td colspan="2" style="text-align:center;height:50px"><input type="submit" value="Search" /></td>
                </tr></table>
            </form>"""
        index += """</div>"""
        index += self.footer()
        return index


if __name__ == "__main__":

    cherrypy.quickstart(Web())
