#!/usr/bin/env python
###
# @author $Author: sebastiendamaye $
# @version $Revision: 14 $
# @lastmodified $Date: 2011-05-28 13:01:56 +0200 (Sat, 28 May 2011) $
#

import ConfigParser

class EvasionTechniques():
    def __init__(self, target, cnf):
        # Read configuration
        self.config = ConfigParser.RawConfigParser()
        self.config.read(cnf)

        self._target = target
        self.payloads = []

    def getPayloads(self):

        ### Nmap decoy test (6th position)
        self.payloads.append([
            "Nmap decoy test (6th position)",
            "command",
            """%sudo% %nmap% -sS -A -D 192.168.100.1,192.168.100.2,192.168.100.3,192.168.100.4,192.168.100.5,ME %target%""",
            "122:2:|1:(2009358|2018489|2100629):",
            "1:(1418|1421|2001219|2002383|2002910|2002911|2003068|2010935|2010936|2010937|2010939|2101228|2101418):",
            "1:(1390|2101390):"
            ])

        ### Nmap decoy test (7th position)
        self.payloads.append([
            "Nmap decoy test (7th position)",
            "command",
            """%sudo% %nmap% -sS -A -D 192.168.100.1,192.168.100.2,192.168.100.3,192.168.100.4,192.168.100.5,192.168.100.6,ME %target%""",
            "122:2:|1:(2009358|2018489|2100629):",
            "1:(2001219|2002383|2002910|2002911|2003068|2010935|2010936|2010937|2010939):",
            "1:(1390|2101390):"
            ])

        ### Hex encoding
        self.payloads.append([
            "Access /etc/passwd with hex encoding",
            "socket",
            80,
            'tcp',
            """GET /index.php?page=%2e%2e%2f%2e%2e%2f%2e%2e%2f%65%74%63%2f%70%61%73%73%77%64 HTTP/1.1\r\nHost: 127.0.0.1\r\nUser-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041202 Firefox/1.0\r\n\r\n""",
            "1:1122:"
            ])

        # UTF8 obfuscation (https://code.google.com/p/teenage-mutant-ninja-turtles/wiki/AdvancedObfuscationPathtraversal)
        self.payloads.append([
            "Access /etc/passwd with UTF8 encoding (1)",
            "socket",
            80,
            'tcp',
            """GET /index.php?page=%c0%2e%c0%2e%c0%af%e0%40%ae%c0ae%e0%80%afetc%e0%80%afpasswd HTTP/1.1\r\nHost: 127.0.0.1\r\nUser-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041202 Firefox/1.0\r\n\r\n""",
            "1:(1122|2002567|2101122):"
            ])

        # UTF8 obfuscation (https://code.google.com/p/teenage-mutant-ninja-turtles/wiki/AdvancedObfuscationPathtraversal)
        self.payloads.append([
            "Access /etc/passwd with UTF8 encoding (2)",
            "socket",
            80,
            'tcp',
            """GET /index.php?page=%c0%2e%c0%2e%c0%af%e0%40%ae%c0ae%e0%80%afetc%c0%2fpasswd HTTP/1.1\r\nHost: 127.0.0.1\r\nUser-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041202 Firefox/1.0\r\n\r\n""",
            "1:(1122|2002567|2101122):"
            ])

        # UTF8 obfuscation (to generate overlong encodings one can use https://github.com/sqlmapproject/sqlmap/blob/master/tamper/overlongutf8.py)
        self.payloads.append([
            "Access /etc/passwd with UTF8 encoding (3)",
            "socket",
            80,
            'tcp',
            """GET /index.php?page=%c0%2e%c0%2e%c0%af%e0%40%ae%c0ae%e0%80%afetc%c0%af%C0%FA%C0%EB%C0%FB%C0%FB%C0%FF%C0%EE HTTP/1.1\r\nHost: 127.0.0.1\r\nUser-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041202 Firefox/1.0\r\n\r\n""",
            "1:(1122|2002567|2101122):"
            ])

        ### Nmap scan with fragmentation
        self.payloads.append([
            "Nmap scan with fragmentation",
            "command",
            """%sudo% %nmap% -PN -sS -A -f %target%""",
            "122:(1|2):|1:2009358:",
            "1:(2002383|2002910|2002911|2003068|2010935|2010936|2010937|2010939|2101228):|123:(8|12|13):",
            "1:(1390|2101390):"
            ])

        ### Nikto Random URI encoding
        self.payloads.append([
            "Nikto Random URI encoding",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 1""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd|1:2002677:",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Directory self reference
        self.payloads.append([
            "Nikto Directory self reference",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 2""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Premature URL ending
        self.payloads.append([
            "Nikto Premature URL ending",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 3""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Prepend long random string
        self.payloads.append([
            "Nikto Prepend long random string",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 4""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd|119:15:",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Fake paramater
        self.payloads.append([
            "Nikto Fake parameter",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 5""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto TAB as request spacer
        self.payloads.append([
            "Nikto TAB as request spacer",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 6""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Change the case of the URL
        self.payloads.append([
            "Nikto Change the case of the URL",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 7""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd|1:2002677:",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Windows directory separator
        self.payloads.append([
            "Nikto Windows directory separator",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion 8""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Carriage return as request spacer
        self.payloads.append([
            "Nikto Carriage return as request spacer",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion A""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd|119:33:",
            "",
            "1:(2016141|2018752):"
            ])

        ### Nikto Binary value as request spacer
        self.payloads.append([
            "Nikto Binary value as request spacer",
            "command",
            """%sudo% %nikto% -config %niktoconf% -h %target% -Plugins cgi -evasion B""",
            "\\.htaccess|\\.htr|\\.ida|\\.idq|\\.cnf|\\.htpasswd",
            "",
            "1:(2016141|2018752):"
            ])

        ### Javascript obfuscation
        self.payloads.append([
            "Javascript Obfuscation",
            "socket",
            80,
            "tcp",
            """GET /index.php?page=%sCscript%3Ealert%28%29%3C%2Fscript%3E HTTP/1.1\r\nHost: 127.0.0.1\r\n\r\n""",
            "1:2009714:"
            ])

        return self.payloads

if __name__ == "__main__":
    print EvasionTechniques("192.168.100.48").getPayloads()